package com.chitresh.user.mindvalley_chitresh_android_test;

import com.chitresh.lib.RequestManager;
import com.chitresh.model.JsonModel;
import com.chitresh.model.ProfileImage;
import com.chitresh.model.User;

import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class AsyncDownloaderUnitTest {
    @Test
    public void objectItem_isSetValid() throws Exception {

        JsonModel json = buildDummyModel();

        ObjectItem item = new ObjectItem(json);

        assertEquals("1", item.getItemTag());
        assertEquals("Testing Name", item.getItemName());
        assertEquals("Testing UserName", item.getItemDesc());
        assertEquals("url", item.getImageURL());

    }


    private JsonModel buildDummyModel() {

        ProfileImage profileImage = new ProfileImage();
        profileImage.setSmall("url");

        User user = new User();
        user.setId("1");
        user.setName("Testing Name");
        user.setUsername("Testing UserName");
        user.setProfileImage(profileImage);

        JsonModel jsonModel = new JsonModel();
        jsonModel.setUser(user);

        return jsonModel;
    }

    @Test
    public void testRequestManager() throws Exception {

        String url1 = "url1";

        RequestManager requestManager = buildDummyRequestManager();

        Object obj1 = new Object();

        //Test with Request
        assertTrue(requestManager.isNewRequest(url1));
        requestManager.updateRequestMap(url1, obj1, 1);
        assertFalse(requestManager.isNewRequest(url1));
        requestManager.cancelRequestMap(url1);
        assertTrue(requestManager.isNewRequest(url1));
        assertTrue(requestManager.isNewRequest(url1));

        //Request with counter 2

        requestManager.updateRequestMap(url1, obj1, 2);
        assertFalse(requestManager.isNewRequest(url1));
        requestManager.cancelRequestMap(url1);
        assertFalse(requestManager.isNewRequest(url1));
        requestManager.cancelRequestMap(url1);
        assertTrue(requestManager.isNewRequest(url1));

        //End Request with counter 2

    }


    private RequestManager buildDummyRequestManager() {

        HashMap<String, HashMap<Integer, Object>> requestMap = new HashMap<String, HashMap<Integer, Object>>();

        RequestManager requestManager = new RequestManager(requestMap);

        return requestManager;

    }


}