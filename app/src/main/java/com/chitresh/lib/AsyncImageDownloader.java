package com.chitresh.lib;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import java.util.HashMap;

/**
 * Created by User
 */

public class AsyncImageDownloader implements AsyncResourceDownloader {


    public static final AsyncImageDownloader imageDownloader = new AsyncImageDownloader();
    private String TAG = this.getClass().getName();

    HashMap<String, HashMap<Integer, Object>> requestMap;
    RequestManager requestManager;

    //Make constructor private
    private AsyncImageDownloader() {
        requestMap = new HashMap<String, HashMap<Integer, Object>>();
        requestManager = new RequestManager(requestMap);
    }

    @Override
    public Object getResource(Context context, String url, Object object) {

        RequestCreator requestCreator = null;
        int key;

        //Check the request is new
        if (requestManager.isNewRequest(url)) {

            //Use of Picasso library for fetching image and in memory caching
            requestCreator = Picasso.with(context).load(url);
            key = 1;

            requestManager.updateRequestMap(url, requestCreator, key);
        } else {

            //Get the request along the requestCreator object
            HashMap<Integer, Object> request = requestManager.getRequestMap().get(url);

            //Get key array
            Object[] keyArray = request.keySet().toArray();
            key = 0;

            //Check for length
            if (keyArray.length > 0) {
                key = (int) request.keySet().toArray()[0];
                requestCreator = (RequestCreator) request.get(key);
            }
            if (requestCreator == null) {
                requestCreator = Picasso.with(context).load(url);
            }

            //Update request counter
            requestManager.updateRequestMap(url, requestCreator, ++key);

        }

        try {
            //Set the imageView object if object is not null
            if (object != null) {
                requestCreator.into((ImageView) object);
            }
        } catch (Exception ex) {
            Log.e(TAG, ex.toString());
        }
        return requestCreator;
    }


    @Override
    public boolean setMaxCapacity(Context context, long max) {
        try {

            //Create new instance of Picasso library with new max capacity
            Picasso picasso = new Picasso.Builder(context).downloader(new OkHttpDownloader(context.getCacheDir(), max)).build();
            Picasso.setSingletonInstance(picasso);
            return true;

        } catch (Exception ex) {
            Log.e(TAG, ex.toString());
            return false;
        }
    }

    /*
    * Cancel request
    * */
    @Override
    public boolean cancel(Context context, String url, Object object) {
        try {
            ImageView imageView = (ImageView) object;

            //Cancel request with Picasso library
            Picasso.with(context)
                    .cancelRequest(imageView);

            //Remove request from request manager
            requestManager.cancelRequestMap(url);

            return true;
        } catch (Exception ex) {
            Log.e(TAG, ex.toString());
            return false;
        }
    }
}
