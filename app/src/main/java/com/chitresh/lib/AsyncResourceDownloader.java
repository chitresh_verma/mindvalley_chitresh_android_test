package com.chitresh.lib;


import android.content.Context;

/**
 * Created by User
 */

public interface AsyncResourceDownloader{

    /**
     * Returns the object of depending on implementation.
     *
     * @param context Context object
     * @param url URL for requested resource
     * @param object Object in which request has any special requirement or user can pass simply null
     *
     * @return Object which can used as per requirement
     *
     */
    public Object getResource(Context context, String url, Object object);


    /**
     * Set the max capacity of requested resource
     *
     * @param context Context object
     * @param max New maximum capacity for resource
     *
     * @return Boolean value to indicate operation was successful or not
     *
     */
    public boolean setMaxCapacity(Context context, long max);


    /**
     * Cancel the request
     *
     * @param context Context object
     * @param url URL for requested resource
     * @param object Object in which request has any special requirement or user can pass simply null
     *
     * @return Boolean value to indicate operation was successful or not
     *
     */
    public boolean cancel(Context context, String url, Object object);

}
