package com.chitresh.lib;

import android.content.Context;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import cz.msebera.android.httpclient.Header;


/**
 * Created by User
 */

public class AsyncJsonDownloader implements AsyncResourceDownloader {


    public static final AsyncJsonDownloader jsonDownloader = new AsyncJsonDownloader();
    private String TAG = this.getClass().getName();

    private TextView dummyTextView;
    private int max = 1000000000;

    //Make Constructor private
    private AsyncJsonDownloader() {
    }

    @Override
    public Object getResource(Context context, String url, Object object) {

        dummyTextView = (TextView) object;

        //Call the URL using LoopJ library(AsyncHttpClient)
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
                dummyTextView.setText("Loading...");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"

                //Convert byte array to string object
                String json = new String(response);

                //Set the response and call the on click event if response is less than max capacity
                if (response.length < max) {
                    dummyTextView.setText(json);
                    dummyTextView.callOnClick();

                } else {
                    Log.i(TAG, "Beyond the max capacity");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                dummyTextView.setText("Request failed: " + statusCode);
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

        return client;
    }


    @Override
    public boolean setMaxCapacity(Context context, long max) {
        try {
            //Set max capacity
            this.max = (int) max;
            return true;
        } catch (Exception ex) {
            Toast.makeText(context, ex.toString(), Toast.LENGTH_SHORT).show();
            return false;
        }

    }

    @Override
    public boolean cancel(Context context, String url, Object object) {

        try {
            //Cancel the request
            AsyncHttpClient client = (AsyncHttpClient) object;
            client.cancelAllRequests(true);
            return true;
        } catch (Exception ex) {
            Toast.makeText(context, ex.toString(), Toast.LENGTH_SHORT).show();
            return false;
        }
    }
}
