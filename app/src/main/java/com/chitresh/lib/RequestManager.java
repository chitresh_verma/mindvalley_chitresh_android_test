package com.chitresh.lib;

import java.util.HashMap;

/**
 * Created by User
 */

public class RequestManager {

    private HashMap<String, HashMap<Integer, Object>> requestMap = null;
    int key;

    //Make default constructor private
    private RequestManager() {
    }

    /*
    *  Constructor with HashMap object
    *
    * @param requestMap HashMap object
    *
    */
    public RequestManager(HashMap<String, HashMap<Integer, Object>> requestMap) {
        this.requestMap = requestMap;
    }

    //Validate the URL for new request
    public boolean isNewRequest(String url) {

        HashMap<Integer, Object> request = requestMap.get(url);

        if (request == null) {
            return true;
        }
        return false;
    }

    //Update request hashmap  with respective URL
    public void updateRequestMap(String url, Object object, int requestCount) {

        HashMap<Integer, Object> request = requestMap.get(url);

        //Set new count if request object is not null
        if (request == null) {
            request = new HashMap<Integer, Object>();
            request.put(requestCount, object);
            requestMap.put(url, request);
        } else {
            request.clear();
            request.put(requestCount, object);
        }

    }

    //Cancel the request
    public void cancelRequestMap(String url) {

        Object object = null;
        HashMap<Integer, Object> request = requestMap.get(url);

        //Check request is not null
        if (request != null) {

            //Get key array
            Object[] keyArray = request.keySet().toArray();
            key = 0;

            //Check for length
            if (keyArray.length > 0) {
                key = (int) request.keySet().toArray()[0];
                object = request.get(key);
            }

            //Reduce the request count by 1 if key has 2 or more request
            if (key > 1 && object != null) {
                updateRequestMap(url, object, --key);
            } else {
                requestMap.remove(url);
            }
        }
    }

    //Getter for requestMap property
    public HashMap<String, HashMap<Integer, Object>> getRequestMap() {
        return requestMap;
    }
}
