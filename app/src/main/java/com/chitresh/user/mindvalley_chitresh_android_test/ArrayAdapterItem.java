package com.chitresh.user.mindvalley_chitresh_android_test;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.chitresh.lib.AsyncImageDownloader;

import java.util.ArrayList;

/**
 * Created by User
 */

public class ArrayAdapterItem extends ArrayAdapter<ObjectItem> {

    Context mContext;
    int layoutResourceId;
    ArrayList<ObjectItem> data = null;

    //Constructor
    public ArrayAdapterItem(Context mContext, int layoutResourceId, ArrayList<ObjectItem> data) {

        super(mContext, layoutResourceId, data);

        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        //Check convertView argument is null
        if (convertView == null) {
            // inflate the layout
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }

        // object item based on the position
        ObjectItem objectItem = data.get(position);

        // get the TextView and then set the image
        ImageView imageViewItem = (ImageView) convertView.findViewById(R.id.imageViewListItem);
        AsyncImageDownloader.imageDownloader.getResource(mContext, objectItem.getImageURL(), imageViewItem);


        // get the TextView and then set the text (item name) and tag (item ID) values
        TextView textViewItem = (TextView) convertView.findViewById(R.id.textListItem1);
        textViewItem.setText(objectItem.getItemName());
        textViewItem.setTag(objectItem.getItemTag());


        // get the TextView and then set the description
        TextView descViewItem = (TextView) convertView.findViewById(R.id.textListItem2);
        descViewItem.setText(objectItem.getItemDesc());

        return convertView;

    }

}
