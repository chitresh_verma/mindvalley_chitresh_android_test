package com.chitresh.user.mindvalley_chitresh_android_test;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.chitresh.lib.AsyncJsonDownloader;
import com.chitresh.model.JsonModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 *
 * Test Activity for validating the library
 * */

public class TestActivity extends Activity {

    //URL String
    private final static String URL = "http://pastebin.com/raw/wgkJgazE";

    private String TAG = this.getClass().getName();

    private Activity activity = this;

    private SwipeRefreshLayout swipeContainer;
    private ListView listView1;
    private Button eventListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        initComponent();
        initData();
    }

    /*
    * Initialize component
    * */
    private void initComponent() {

        listView1 = (ListView) findViewById(R.id.listView1);

        // Lookup the swipe container view
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);

        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                //Call method to refresh
                initData();

            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    /*
    * Initialize the data
    */
    private void initData() {

        //Use of button as Event listener
        eventListener = new Button(this);

        //Library calls the on click listener
        eventListener.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setAdapter();

                // Now we call setRefreshing(false) to signal refresh has finished
                swipeContainer.setRefreshing(false);
            }
        });

        //Get JSON using API
        AsyncJsonDownloader.jsonDownloader.getResource(this, URL, eventListener);
    }

    /*
    * Set Json Data to list view
    * */
    private void setAdapter() {

        //Create new instance
        GsonBuilder builder = new GsonBuilder();

        //Use of GSON library to get data
        Type collectionType = new TypeToken<Collection<JsonModel>>() {
        }.getType();
        Collection<JsonModel> enums = builder.create().fromJson(eventListener.getText().toString(), collectionType);

        ArrayList<ObjectItem> objectItems = buildArrayList(enums);

        //adapter instance
        ArrayAdapterItem adapter = new ArrayAdapterItem(activity, R.layout.simple_list_item_with_photo, objectItems);

        //Set Adapter on listview
        listView1.setAdapter(adapter);
    }

    /*
    * Build the array list
    * */
    private ArrayList<ObjectItem> buildArrayList(Collection<JsonModel> enums) {

        ArrayList<ObjectItem> objectItems = new ArrayList<ObjectItem>();

        try {
            // Use iterator
            Iterator itr = enums.iterator();

            while (itr.hasNext()) {

                Object element = itr.next();

                JsonModel jsonModel = (JsonModel) element;
                ObjectItem item = new ObjectItem(jsonModel);

                objectItems.add(item);
            }
        } catch (Exception ex) {
            Log.i(TAG, ex.toString());
        }
        return objectItems;
    }


}
