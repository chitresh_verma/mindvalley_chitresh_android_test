package com.chitresh.user.mindvalley_chitresh_android_test;

import com.chitresh.model.JsonModel;

/**
 * Created by User
 */

public class ObjectItem {

    private String itemTag;
    private String imageURL;
    private String itemName;
    private String itemDesc;

    //Constructor to store the parameters from JsonModel
    public ObjectItem(JsonModel jsonModel) {

        setItemTag(jsonModel.getUser().getId().toString());
        setItemName(jsonModel.getUser().getName());
        setItemDesc(jsonModel.getUser().getUsername());
        setImageURL(jsonModel.getUser().getProfileImage().getSmall().toString());

    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemTag() {
        return itemTag;
    }

    public void setItemTag(String itemTag) {
        this.itemTag = itemTag;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }


}
